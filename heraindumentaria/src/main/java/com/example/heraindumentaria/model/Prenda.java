package com.example.heraindumentaria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Prenda implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, updatable = false)
	private Long id;
	private String descripcion;
	private float precioCompra;
	private float precioVenta;
	private float diferenciaPrecio;
	private boolean pago;
	private float pagoParcial;
	private String nombreComprador;
	private String nombreVendedor;
	@Column(nullable = false, updatable = false)
	private String prendaCode;

	public Prenda(Long id, String descripcion, float precioCompra, float precioVenta, float diferenciaPrecio,
			boolean pago, float pagoParcial, String nombreComprador, String nombreVendedor, String prendaCode) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
		this.diferenciaPrecio = diferenciaPrecio;
		this.pago = pago;
		this.pagoParcial = pagoParcial;
		this.nombreComprador = nombreComprador;
		this.nombreVendedor = nombreVendedor;
		this.prendaCode = prendaCode;
	}

	public Prenda() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public float getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(float precioCompra) {
		this.precioCompra = precioCompra;
	}

	public float getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(float precioVenta) {
		this.precioVenta = precioVenta;
	}

	public float getDiferenciaPrecio() {
		return diferenciaPrecio;
	}

	public void setDiferenciaPrecio(float diferenciaPrecio) {
		this.diferenciaPrecio = diferenciaPrecio;
	}

	public boolean isPago() {
		return pago;
	}

	public void setPago(boolean pago) {
		this.pago = pago;
	}

	public float getPagoParcial() {
		return pagoParcial;
	}

	public void setPagoParcial(float pagoParcial) {
		this.pagoParcial = pagoParcial;
	}

	public String getNombreComprador() {
		return nombreComprador;
	}

	public void setNombreComprador(String nombreComprador) {
		this.nombreComprador = nombreComprador;
	}

	public String getNombreVendedor() {
		return nombreVendedor;
	}

	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

	public String getPrendaCode() {
		return prendaCode;
	}

	public void setPrendaCode(String prendaCode) {
		this.prendaCode = prendaCode;
	}

}
