package com.example.heraindumentaria.exception;

public class PrendaNoEncontradaException extends RuntimeException {
	public PrendaNoEncontradaException(String mensaje) {
		super(mensaje);
	}

}
