package com.example.heraindumentaria;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.heraindumentaria.model.Prenda;
import com.example.heraindumentaria.service.PrendaService;


@RestController
@RequestMapping("/prenda")
public class PrendaResource {
	private final PrendaService prendaService;

	public PrendaResource(PrendaService prendaService) {
		this.prendaService = prendaService;
	}

	@GetMapping("/all")
	public ResponseEntity<List<Prenda>> getAllPrendas() {
		List<Prenda> prendas = prendaService.findAllPrendas();
		return new ResponseEntity<>(prendas, HttpStatus.OK);
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Prenda> getPrendaById(@PathVariable("id") Long id) {
		Prenda prenda = prendaService.findPrendaById(id);
		return new ResponseEntity<>(prenda, HttpStatus.OK);
	}

	@PostMapping("/add")
	public ResponseEntity<Prenda> addPrenda(@RequestBody Prenda prenda) {
		Prenda newPrenda = prendaService.addPrenda(prenda);
		return new ResponseEntity<>(newPrenda, HttpStatus.CREATED);
	}

	@PutMapping("/update")
	public ResponseEntity<Prenda> updatePrenda(@RequestBody Prenda prenda) {
		Prenda updatePrenda = prendaService.updatePrenda(prenda);
		return new ResponseEntity<>(updatePrenda, HttpStatus.OK);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deletePrenda(@PathVariable("id") Long id) {
		prendaService.deletePrenda(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
