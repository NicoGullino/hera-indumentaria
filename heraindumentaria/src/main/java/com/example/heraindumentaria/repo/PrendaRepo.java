package com.example.heraindumentaria.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.heraindumentaria.model.Prenda;

public interface PrendaRepo extends JpaRepository<Prenda, Long> {

	void deletePrendaById(Long id);

	Optional<Prenda> findPrendaById(Long id);

}
