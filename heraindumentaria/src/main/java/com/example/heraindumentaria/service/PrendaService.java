package com.example.heraindumentaria.service;

import java.util.List;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.heraindumentaria.exception.PrendaNoEncontradaException;
import com.example.heraindumentaria.model.Prenda;
import com.example.heraindumentaria.repo.PrendaRepo;

@Service
@Transactional
public class PrendaService {
	private final PrendaRepo prendaRepo;

	@Autowired
	public PrendaService(PrendaRepo prendaRepo) {
		this.prendaRepo = prendaRepo;
	}

	public Prenda addPrenda(Prenda prenda) {
		prenda.setPrendaCode(UUID.randomUUID().toString());
		return prendaRepo.save(prenda);
	}

	public List<Prenda> findAllPrendas() {
		return prendaRepo.findAll();
	}

	public Prenda findPrendaById(Long id) {
		return prendaRepo.findPrendaById(id)
				.orElseThrow(() -> new PrendaNoEncontradaException("Prenda por id" + id + "no fue encontrado"));
	}

	public Prenda updatePrenda(Prenda prenda) {
		return prendaRepo.save(prenda);
	}

	public void deletePrenda(Long id) {
		prendaRepo.deletePrendaById(null);
	}
}
